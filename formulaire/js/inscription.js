$(function () {

    $.validator.addMethod("PWCHECK",
        function (value, element) {
            if (/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&#=()]){1,}).{8,}$/.test(value)){
                return true;
            }else{
                return false;
            }

        }
    );



    $('#inscription_form').validate(
        {
            rules:{
                nom_per:{
                    required: true,
                    minlength: 2
                },
                prenom_per:{
                    required: true,
                    minlength:2
                },
                mail_per:"required",
                mdp_per:{
                    required: true,
                    minlength: 8,
                    PWCHECK: true
                },
                mdp2_per:{
                    required:true,
                    equalTo:mdp_per
                }

            },
            messages:{
                nom_per: {
                    required:"Veuillez saisir votre nom",
                    minlength:"Votre nom doit être composé de 2 caractères au minimum"
                },
                prenom_per:{
                    required:"Veuillez saisir votre prénom",
                    minlength:"Votre prénom doit être composé de 2 caractères au minimum"
                },
                mail_per:{
                    required:"Veuillez saisir votre adresse e-mail",
                    email:"Votre adresse doit être une adresse e-mail valide"
                },
                mdp_per: {
                    required:"Veuillez saisir votre mot de passe",
                    minlength:"Votre mot de passe doit être composé de 8 caractères au minimum",
                    PWCHECK: "Le mot de passse doit comporter au minium une minuscule, un chiffre, une majuscule et un charactère spécial"
                },
                mdp2_per: {
                    equalTo: "Les mots de passe ne sont pas identiques",
                    required:"Veuillez saisir une deuxième fois votre mot de passe"
                }
            }
        }
    )
});
