<div id="modal_inscritpion" class="modal fade" role="dialog" xmlns: xmlns:>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Inscription</h4>
            </div>
            <div class="modal-body">
                <form id="formulaire_inscription" action="./check.php" method="post">
                    <div class="form-group row">
                        <label class="col-md-3 text-right  col-form-label align-middle" for="name">Nom</label>
                        <div class="col-md-7"><input class="form-control" type="text" name="nom" id="name" placeholder="Votre nom"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 text-right col-form-label" for="surname">Prénom</label>
                        <div class="col-md-7"><input class="form-control" type="text" id="surname" name="prénom" placeholder="Votre prénom"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 text-right col-form-label" for="tel">Numéro de téléphone</label>
                        <div class="col-md-7"><input class="form-control" type="tel" id="tel" name="numero" placeholder="Votre numéro de téléphone"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 text-right col-form-label" for="email">E-mail</label>
                        <div class="col-md-7"><input class="form-control" type="email" id="email" name="email" placeholder="Votre adresse e-mail"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 text-right col-form-label" for="teacher">Moniteur</label>
                        <div class="col-md-1"><input class="col-md-1 form-control" type="checkbox" name="moniteur" id="teacher"></div>
                        <label class="col-md-3 col-form-label" for="nb_student">Nombre d'élèves</label>
                        <div class="col-md-3"><input class="form-control" type="number" name="nbeleve" id="nb_student"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 text-right col-form-label" for="activity">Activité</label>
                        <div class="col-md-1"><input class="form-control" type="radio" id="nageur" name="activité"></div>
                        <label class="col-md-4" for id="Nageur">Nageur</label>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-1"><input class="form-control" type="radio" id="apneiste" name="activité"></div>
                        <label class="col-md-4" for id="Apneiste">Apneiste</label>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-1"><input class="form-control" type="radio" id="plongeur" name="activité"></div>
                        <label class="col-md-4" for id="Plongeur">Plongeur</label>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 text-right col-form-label" for="status">Statut</label>
                        <div class="col-md-1"><input class="form-control" type="radio" id="Club" name="status"></div>
                        <label class="col-md-3" forid="Club">Club/école</label>
                        <div class="col-md-3"><input class="form-control" type="text" id="clubname" name="nomduclub"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-1"><input class="form-control" type="radio" id="private" name="status"></div>
                        <label class="col-md-4" id="private">Privé</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">Annuler</button>
                        <input type="submit" class="btn btn-primary" id="submit_conf" value="S'inscrire">
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<script src="./js/octopus.js"></script>