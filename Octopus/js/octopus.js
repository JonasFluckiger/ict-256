$(function () {
    $( "#nb_student" ).prop( "disabled", true );
    $("[name=activité]:checked").val()
    $('#teacher').click(function(){
        if($(this).prop("checked") == true){
            $( "#nb_student" ).prop( "disabled", false );
        }
        else if($(this).prop("checked") == false){
            $( "#nb_student" ).prop( "disabled", true );
        }
    });
    $( "#clubname" ).prop( "disabled", true );
    $('#Club').click(function(){
        if($(this).prop("checked") == true){
            $( "#clubname" ).prop( "disabled", false );
        }
        else if($(this).prop("checked") == false){
            $( "#clubname" ).prop( "disabled", true );
        }
    });
    $('#private').click(function(){
        if($(this).prop("checked") == true){
            $( "#clubname" ).prop( "disabled", true );
        }
        else if($(this).prop("checked") == false){
            $( "#clubname" ).prop( "disabled", false );
        }
    });
$('#formulaire_inscription').validate({

    rules: {
        nom: {
            minlength: 2
        },
        prénom: {
            required: true,
            minlength: 2
        },
        email:{

        },
    },
    messages: {
        nom: {
            minlength: "Votre nom doit être composé de 2 caractères au minimum"
        },
        prénom:{
            required:"Veuillez saisir votre prénom",
            minlength:"Votre prénom doit être composé de 2 caractères au minimum"
        },
        email: {
            required: "Veuillez saisir votre adresse e-mail",
            email: "Votre adresse doit être une adresse e-mail valide"
            }
        }

    });
});